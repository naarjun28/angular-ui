import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TradingService } from './trading.service';


@Injectable({
  providedIn: 'root'
})
export class RiskanalyserService {

  serviceURL = 'https://o8we309jc7.execute-api.eu-west-1.amazonaws.com/default/riskAnalysis_DUB2?ticker='
  constructor(private http: HttpClient) { }
  analyseRisk(ticker: string) {
    let fullURL = `${this.serviceURL}${ticker}`
    return this.http.get(fullURL)
  }
}
