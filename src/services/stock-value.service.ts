import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StockValueService {

  serviceURL = 'https://8kut9rz75k.execute-api.eu-west-1.amazonaws.com/default/livePrice_DUB2?ticker='
  constructor(private http: HttpClient) { }

  getLivePrice(ticker: string) {
    let Url = `${this.serviceURL}${ticker}`
    return this.http.get(Url)
  }
}
