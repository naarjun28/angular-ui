import { TestBed } from '@angular/core/testing';

import { StockValueService } from './stock-value.service';

describe('StockValueService', () => {
  let service: StockValueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockValueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
