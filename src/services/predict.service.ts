import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  serviceURL = 'https://biupje6cqh.execute-api.eu-west-1.amazonaws.com/default/stockPredictor_DUB2?ticker='
  constructor(private http: HttpClient) { }

  getPrediction(ticker: string) {
    let fullURL = `${this.serviceURL}${ticker}`
    return this.http.get(fullURL)
  }
}
