import { Injectable } from '@angular/core';
import { FinService } from './fin.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Injectable({
  providedIn: 'root'
})
export class InvestmentCalculationService {

  constructor(private service: FinService, private snackbar: MatSnackBar) { }

  calculateInvestment(ticker: string, quantity) {
    let price
    let requestedPrice
    let message = `The final investment in ${ticker} is`
    this.service.getStocks(ticker, "1")
      .subscribe((response) => {
        price = response
        requestedPrice = quantity * price.price_data[0].price
        this.snackbar.open(message, requestedPrice, {
          duration: 8000,
        });


      })

  }
}
