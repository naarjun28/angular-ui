import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TradingService {

  constructor(private http: HttpClient) { }
  // serviceURL = ' http://localhost:8080/trades/getAllStocks'
  serviceURL = 'http://citieurlinux2.conygre.com:8080/trades/getAllStocks'
  getStocks() {
    return this.http.get(this.serviceURL)
  }
}
