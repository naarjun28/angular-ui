import { TestBed } from '@angular/core/testing';

import { RiskanalyserService } from './riskanalyser.service';

describe('RiskanalyserService', () => {
  let service: RiskanalyserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RiskanalyserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
