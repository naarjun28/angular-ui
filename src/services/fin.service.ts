import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FinService {

  serviceURL = ' https://agez3b6adi.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed2?ticker='
  someParams = '&num_days='
  constructor(private http: HttpClient) { }
  getStocks(code: string, days: string) {
    let fullUrl = `${this.serviceURL}${code}${this.someParams}${days}`
    return this.http.get(fullUrl)
  }
}
