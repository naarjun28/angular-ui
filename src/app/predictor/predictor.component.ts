import { Component, OnInit } from '@angular/core';
import { PredictService } from 'src/services/predict.service';


interface ticker {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-predictor',
  templateUrl: './predictor.component.html',
  styleUrls: ['./predictor.component.css']
})
export class PredictorComponent implements OnInit {

  constructor(private predictService:PredictService) { }
  ticker = 'C'
  data
  URL:string
  ngOnInit(): void {
    this.predictService.getPrediction(this.ticker)
    .subscribe((response)=>{ 
      this.data = response
      
      if(this.data.Decision == 'BUY'){
        this.URL = '../assets/buy.png'
      }
      else if(this.data.Decision == 'HOLD'){
        this.URL = '../assets/hold.png'
      }
      else if(this.data.Decision == 'SELL'){
        this.URL = '../assets/sell.png'
      }
      

    })
    
  }

  predictInvestment(): void{
    this.predictService.getPrediction(this.ticker)
    .subscribe((response)=>{ this.data = response
      if(this.data.Decision == 'BUY'){
        this.URL = '../assets/buy.png'
      }
      else if(this.data.Decision == 'HOLD'){
        this.URL = '../assets/hold.png'
      }
      else if(this.data.Decision == 'SELL'){
        this.URL = '../assets/sell.png'
      }
    })
  }
  
  tickers: ticker[] = [
    { value: 'C', viewValue: 'Citi' },
    { value: 'AMZN', viewValue: 'Amazon' },
    { value: 'AAPL', viewValue: 'Apple Inc' },
    { value: 'MSFT', viewValue: 'Microsoft' },
    { value: 'GOOG', viewValue: 'Google' },
    { value: 'FB', viewValue: 'Facebook' }]
  selectedticker = this.tickers[0].value;
  selectCar(event: Event) {
    this.selectedticker = (event.target as HTMLSelectElement).value;
  }

  hidden = 'hidden'
  style = 'display:none;'
  setHidden(){
    this.hidden = 'text'
    this.style='display:block;'
  }

  setNoHidden(){
    this.hidden='hidden'
    this.style = 'display:none;'
  }

}
