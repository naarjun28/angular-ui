import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutTradeAppComponent } from './about-trade-app.component';

describe('AboutTradeAppComponent', () => {
  let component: AboutTradeAppComponent;
  let fixture: ComponentFixture<AboutTradeAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutTradeAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutTradeAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
