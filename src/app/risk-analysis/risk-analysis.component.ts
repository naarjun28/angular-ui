import { Component, OnInit } from '@angular/core';
import { RiskanalyserService } from 'src/services/riskanalyser.service';
import { MatSnackBar } from '@angular/material/snack-bar';

interface ticker {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-risk-analysis',
  templateUrl: './risk-analysis.component.html',
  styleUrls: ['./risk-analysis.component.css']
})
export class RiskAnalysisComponent implements OnInit {

  constructor(private riskService: RiskanalyserService, private snackbar: MatSnackBar) { }
  ticker: string = 'C'
  data
  showSpinner: boolean = true
  message: string = 'Analysing for ticker'
  start_price
  mean_final_price
  risk_value
  percentage_risk
  risk_style
  ngOnInit(): void {
    this.snackbar.open(this.message, this.ticker, {
      duration: 4000,
    });
    this.riskService.analyseRisk(this.ticker)
      .subscribe((response) => {
        this.data = response
        this.start_price = this.data.start_price
        this.mean_final_price = this.data.mean_final_price
        this.risk_value = this.data.risk_value
        this.percentage_risk = this.data.percentage_risk
        this.showSpinner = false
        if(this.percentage_risk >= 7)
        {
          this.risk_style = 'color:red;'
        }
        else{    this.risk_style = 'color:#4CAF50;'    }
      })
  }


  predictRisk(): void {
    this.showSpinner = true
    this.snackbar.open(this.message, this.ticker, {
      duration: 4000,
    });
    this.riskService.analyseRisk(this.ticker)
      .subscribe((response) => {
        this.data = response
        this.start_price = this.data.start_price
        this.mean_final_price = this.data.mean_final_price
        this.risk_value = this.data.risk_value
        this.percentage_risk = this.data.percentage_risk
        this.showSpinner = false
        if(this.percentage_risk >= 7)
        {
          this.risk_style = 'color:red;'
        }
        else{    this.risk_style = 'color:#4CAF50;'    }
      })
  }

  tickers: ticker[] = [
    { value: 'C', viewValue: 'Citi' },
    { value: 'AMZN', viewValue: 'Amazon' },
    { value: 'AAPL', viewValue: 'Apple Inc' },
    { value: 'MSFT', viewValue: 'Microsoft' },
    { value: 'GOOG', viewValue: 'Google' },
    { value: 'FB', viewValue: 'Facebook' }]
  selectedticker = this.tickers[0].value;
  selectCar(event: Event) {
    this.selectedticker = (event.target as HTMLSelectElement).value;
  }

  hidden = 'hidden'
  style = 'display:none;'
  setHidden() {
    this.hidden = 'text'
    this.style = 'display:block;'
  }

  setNoHidden() {
    this.hidden = 'hidden'
    this.style = 'display:none;'
  }


}
