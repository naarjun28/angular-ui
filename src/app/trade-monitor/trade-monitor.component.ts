import { Component, OnInit } from '@angular/core';
import { TradingService } from 'src/services/trading.service';
import { DatePipe } from '@angular/common';
import { FinService } from 'src/services/fin.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InvestmentCalculationService } from 'src/services/investment-calculation.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface tradeState {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-trade-monitor',
  templateUrl: './trade-monitor.component.html',
  styleUrls: ['./trade-monitor.component.css']
})
export class TradeMonitorComponent implements OnInit {

  mydate: any = new Date();
  constructor(private tradeService: TradingService, private date: DatePipe, private finservice: FinService, private snackbar: MatSnackBar, private invest: InvestmentCalculationService, private http: HttpClient) {
    this.mydate = this.date.transform(this.mydate, 'MM/dd/yyyy');
  }
  data
  stockTicker
  stockQuantity
  requestedPrice
  tradeStates = 'BUY'
    
  postURL = 'http://citieurlinux2.conygre.com:8080/trades/addStock'
  ngOnInit(): void {
    this.tradeService.getStocks()
      .subscribe((response) => { this.data = response })
  }
  

  getStocks(): void {
    this.tradeService.getStocks()
      .subscribe((response) => { this.data = response })
  }
  timerId = setInterval(() => this.getStocks(), 2000)

  
  tradestate: tradeState[] = [
    { value: 'BUY', viewValue: 'Buy' },
    { value: 'SELL', viewValue: 'Sell' }
  ]
  selectedstate = this.tradestate[0].value;
  selectCar(event: Event) {
    this.selectedstate = (event.target as HTMLSelectElement).value;
  }
  displayColumns: string[] = ['Date', 'Ticker', 'Quantity', 'Price', 'State', 'Type'];

  postStock() {
    let price
    let message = `Your investment in ${this.stockTicker} is`
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json' // no auhentication this time
      })
    };
    let data
    this.finservice.getStocks(this.stockTicker, "1")
      .subscribe((response) => {
        price = response
        this.requestedPrice = this.stockQuantity * price.price_data[0].price
        this.snackbar.open(message, this.requestedPrice, {
          duration: 8000,
        });
        data = `{
        "date": "${this.mydate}",
        "stockTicker": "${this.stockTicker}",
        "stockQuantity": "${this.stockQuantity}",
        "requestedPrice": "${this.requestedPrice}",
        "tradeType": "${this.tradeStates}"
      }`
        console.log(data)
        this.http.post(this.postURL, data, httpOptions)
          .subscribe((response) => {

          })
      })


  }
  checkInvestment(): void {

    this.invest.calculateInvestment(this.stockTicker, this.stockQuantity)

  }

}
