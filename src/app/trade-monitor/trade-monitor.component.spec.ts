import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeMonitorComponent } from './trade-monitor.component';

describe('TradeMonitorComponent', () => {
  let component: TradeMonitorComponent;
  let fixture: ComponentFixture<TradeMonitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradeMonitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
