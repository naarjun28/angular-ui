import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutTradeAppComponent } from './about-trade-app/about-trade-app.component';
import { TradeMonitorComponent } from './trade-monitor/trade-monitor.component';
import { StockMarketComponent } from './stock-market/stock-market.component';
import { PredictorComponent } from './predictor/predictor.component';
import { RiskAnalysisComponent } from './risk-analysis/risk-analysis.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'aboutTradingApp'
  },
  {
    path: 'aboutTradingApp',
    component: AboutTradeAppComponent
  },
  {
    path: 'tradeMonitor',
    component: TradeMonitorComponent
  },
  {
    path: 'stockMarket',
    component: StockMarketComponent
  },
  {
    path: 'predictor',
    component: PredictorComponent
  },
  {
    path: 'riskAnalysis',
    component: RiskAnalysisComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
