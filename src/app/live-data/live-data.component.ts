import { Component, OnInit } from '@angular/core';
import { StockValueService } from 'src/services/stock-value.service';

@Component({
  selector: 'app-live-data',
  templateUrl: './live-data.component.html',
  styleUrls: ['./live-data.component.css']
})
export class LiveDataComponent implements OnInit {

  constructor(private stockService:StockValueService) { }
  citi_data:any = { "High": 46.7599983215332, "Low": 44.619998931884766, "Open": 45.209999084472656, "Close": 46.02000045776367, "Volume": 45039745, "Adj_Close": 46.02000045776367 }
  apple_data:any = { "High": 46.7599983215332, "Low": 44.619998931884766, "Open": 45.209999084472656, "Close": 46.02000045776367, "Volume": 45039745, "Adj_Close": 46.02000045776367 }
  amazon_data:any = { "High": 46.7599983215332, "Low": 44.619998931884766, "Open": 45.209999084472656, "Close": 46.02000045776367, "Volume": 45039745, "Adj_Close": 46.02000045776367 }
  google_data:any = { "High": 46.7599983215332, "Low": 44.619998931884766, "Open": 45.209999084472656, "Close": 46.02000045776367, "Volume": 45039745, "Adj_Close": 46.02000045776367 }

  ngOnInit(): void {
    this.stockService.getLivePrice("C")
    .subscribe((res)=> {this.citi_data = res})

    this.stockService.getLivePrice("AAPL")
    .subscribe((res)=> {this.apple_data = res})

    this.stockService.getLivePrice("AMZN")
    .subscribe((res)=> {this.amazon_data = res})

    this.stockService.getLivePrice("GOOG")
    .subscribe((res)=> {this.google_data = res})
  }

  timerId = setInterval(() => this.ngOnInit(), 300000 )


}
