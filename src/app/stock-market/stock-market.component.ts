import { Component, OnInit } from '@angular/core';
import { FinService } from 'src/services/fin.service';

interface ticker {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-stock-market',
  templateUrl: './stock-market.component.html',
  styleUrls: ['./stock-market.component.css']
})
export class StockMarketComponent implements OnInit {

  constructor(private myfinService: FinService) { }
  code: string = 'C'
  days: string = '10'
  data
  ngOnInit(): void {
    this.myfinService.getStocks(this.code, this.days)
      .subscribe((response) => { this.data = response
        this.plotChart(this.data)
      })

  }
  getPrice(): void {
    this.myfinService.getStocks(this.code, this.days)
      .subscribe((response) => { this.data = response
        this.plotChart(this.data)
      })
  }

  plotChart(data){

  }



  tickers: ticker[] = [
    { value: 'C', viewValue: 'Citi' },
    { value: 'AMZN', viewValue: 'Amazon' },
    { value: 'AAPL', viewValue: 'Apple Inc' },
    { value: 'MSFT', viewValue: 'Microsoft' },
    { value: 'GOOG', viewValue: 'Google' },
    { value: 'FB', viewValue: 'Facebook' }]
  displayColumns: string[] = ['Date', 'Price'];
  selectedticker = this.tickers[0].value;
  selectCar(event: Event) {
    this.selectedticker = (event.target as HTMLSelectElement).value;
  }


  autoTicks = false;
  disabled = false;
  invert = false;
  max = 730;
  min = 1;
  showTicks = false;
  step = 1;
  thumbLabel = true;
  vertical = false;
  tickInterval = 1;

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }
  }
  hidden = 'hidden'
  style = 'display:none;'
  setHidden() {
    this.hidden = 'text'
    this.style = 'display:block;'
  }

  setNoHidden() {
    this.hidden = 'hidden'
    this.style = 'display:none;'
  }
}
