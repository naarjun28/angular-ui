import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatCardModule} from '@angular/material/card';
import {MatTooltipModule} from '@angular/material/tooltip'
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule, MatTabBody} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatListModule} from '@angular/material/list';
import { AboutTradeAppComponent } from './about-trade-app/about-trade-app.component';
import { LiveDataComponent } from './live-data/live-data.component';
import { TradeMonitorComponent } from './trade-monitor/trade-monitor.component';
import { PredictorComponent } from './predictor/predictor.component';
import { StockMarketComponent } from './stock-market/stock-market.component';
import { RiskAnalysisComponent } from './risk-analysis/risk-analysis.component';
import { DatePipe } from '@angular/common';
import { IgxFinancialChartModule } from "igniteui-angular-charts";


@NgModule({
  declarations: [
    AppComponent,
    AboutTradeAppComponent,
    LiveDataComponent,
    TradeMonitorComponent,
    PredictorComponent,
    StockMarketComponent,
    RiskAnalysisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSliderModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatListModule,
    IgxFinancialChartModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
