import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'easyTrade-UI';

  state:boolean = false

  setState(){
    if(this.state === true){
      this.state = false
    }
    else{
      this.state = true
    }
  }
}
